FROM rocker/r-ver:4.2.1

RUN apt-get update
RUN apt-get install -y --no-install-recommends libxt6
RUN apt-get install nano
RUN apt-get install -y libxml2 libodbc1

ENV S6_VERSION=v2.1.0.2
ENV RSTUDIO_VERSION=2022.07.2+576
ENV DEFAULT_USER=rstudio
ENV PANDOC_VERSION=default
ENV QUARTO_VERSION=default

RUN /rocker_scripts/install_rstudio.sh
RUN /rocker_scripts/install_pandoc.sh
RUN /rocker_scripts/install_quarto.sh
RUN quarto install extension --no-prompt quarto-ext/grouped-tabsets

RUN useradd -u 1001 -g rstudio -m conda
RUN useradd -u 504 -g rstudio -m mac
USER conda

WORKDIR /home/conda

ENV PATH="/home/conda/miniconda3/bin:${PATH}"
ARG PATH="/home/conda/miniconda3/bin:${PATH}"
ENV MLFLOW_BIN=/home/conda/miniconda3/bin/mlflow
ENV MLFLOW_PYTHON_BIN=/home/conda/miniconda3/bin/python

RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh
RUN conda --version

USER root

RUN R -q -e 'install.packages(c("reticulate","vetiver","pins","dplyr","checkmate","quarto","gt","tidymodels","randomForest","tidyverse","lcmm"))'
ENV PATH "$PATH:/root/.local/share/r-miniconda/bin"
RUN R -q -e 'reticulate::py_install(envname="base",c("vetiver","pins","scikit-learn","pandas","nbformat","nbclient"),pip=TRUE)'
ENV USER="rstudio"

CMD ["/usr/lib/rstudio-server/bin/rserver", "--server-daemonize", "0", "--auth-none", "1"]

EXPOSE 8787

CMD ["/init"]





